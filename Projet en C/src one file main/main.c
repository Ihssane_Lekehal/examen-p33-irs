#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_VEHICLES 3000

// Structure représentant un véhicule
struct Vehicule {
    char type[20];
    char immatriculation[20];
    char marque[50];
    char modele[50];
    int annee;
    char couleur[20];
};

// Structure représentant une ville
struct Ville {
    struct Vehicule vehicules[MAX_VEHICLES];
    int nbVehicules;
};

struct Statistiques {
    char marque[50];
    int count;
};

// Fonction pour ajouter un véhicule
void ajouterVehicule(struct Ville* ville) {
    if (ville->nbVehicules == MAX_VEHICLES) {
        printf("La limite du nombre de véhicules est atteinte.\n");
        return;
    }

    struct Vehicule vehicule;

    printf("Type (Voiture, Moto, Autobus ou Metro) : ");
    scanf("%s", vehicule.type);

    printf("Immatriculation : ");
    scanf("%s", vehicule.immatriculation);

    printf("Marque : ");
    scanf("%s", vehicule.marque);

    printf("Modèle : ");
    scanf("%s", vehicule.modele);

    printf("Année : ");
    scanf("%d", &vehicule.annee);

    printf("Couleur : ");
    scanf("%s", vehicule.couleur);

    ville->vehicules[ville->nbVehicules] = vehicule;
    ville->nbVehicules++;

    printf("Le véhicule a été ajouté avec succès.\n");
}

// Fonction pour supprimer un véhicule
void supprimerVehicule(struct Ville* ville) {
    char immatriculation[20];
    printf("Entrez l'immatriculation du véhicule à supprimer : ");
    scanf("%s", immatriculation);

    int vehiculeTrouve = 0;
    int index = -1;

    for (int i = 0; i < ville->nbVehicules; i++) {
        if (strcmp(ville->vehicules[i].immatriculation, immatriculation) == 0) {
            vehiculeTrouve = 1;
            index = i;
            break;
        }
    }

    if (vehiculeTrouve) {
        for (int i = index; i < ville->nbVehicules - 1; i++) {
            ville->vehicules[i] = ville->vehicules[i + 1];
        }
        ville->nbVehicules--;
        printf("Le véhicule a été supprimé avec succès.\n");
    } else {
        printf("Aucun véhicule trouvé avec l'immatriculation fournie.\n");
    }
}

// Fonction pour modifier un véhicule
void modifierVehicule(struct Ville* ville) {
    char immatriculation[20];
    printf("Entrez l'immatriculation du véhicule à modifier : ");
    scanf("%s", immatriculation);

    int vehiculeTrouve = 0;
    int index = -1;

    for (int i = 0; i < ville->nbVehicules; i++) {
        if (strcmp(ville->vehicules[i].immatriculation, immatriculation) == 0) {
            vehiculeTrouve = 1;
            index = i;
            break;
        }
    }

    if (vehiculeTrouve) {
        struct Vehicule* vehicule = &ville->vehicules[index];

        printf("Quelle information souhaitez-vous modifier ?\n");
        printf("1. Type\n");
        printf("2. Immatriculation\n");
        printf("3. Marque\n");
        printf("4. Modèle\n");
        printf("5. Année\n");
        printf("6. Couleur\n");
        printf("Votre choix : ");

        int choix;
        scanf("%d", &choix);

        switch (choix) {
            case 1:
                printf("Nouveau type (%s) : ", vehicule->type);
                scanf("%s", vehicule->type);
                break;
            case 2:
                printf("Nouvelle immatriculation (%s) : ", vehicule->immatriculation);
                scanf("%s", vehicule->immatriculation);
                break;
            case 3:
                printf("Nouvelle marque (%s) : ", vehicule->marque);
                scanf("%s", vehicule->marque);
                break;
            case 4:
                printf("Nouveau modèle (%s) : ", vehicule->modele);
                scanf("%s", vehicule->modele);
                break;
            case 5:
                printf("Nouvelle année (%d) : ", vehicule->annee);
                scanf("%d", &vehicule->annee);
                break;
            case 6:
                printf("Nouvelle couleur (%s) : ", vehicule->couleur);
                scanf("%s", vehicule->couleur);
                break;
            default:
                printf("Choix invalide. Aucune modification effectuée.\n");
                return;
        }

        printf("Le véhicule a été modifié avec succès.\n");
    } else {
        printf("Aucun véhicule trouvé avec l'immatriculation fournie.\n");
    }
}

// Fonction pour afficher tous les véhicules enregistrés
void afficherTousVehicules(struct Ville* ville) {
    printf("----- Liste des véhicules -----\n");
    for (int i = 0; i < ville->nbVehicules; i++) {
        struct Vehicule vehicule = ville->vehicules[i];
        printf("Type: %s\n", vehicule.type);
        printf("Immatriculation: %s\n", vehicule.immatriculation);
        printf("Marque: %s\n", vehicule.marque);
        printf("Modèle: %s\n", vehicule.modele);
        printf("Année: %d\n", vehicule.annee);
        printf("Couleur: %s\n", vehicule.couleur);
        printf("\n");
    }
}

// Fonction pour rechercher un véhicule par immatriculation
void rechercherVehicule(struct Ville* ville) {
    char immatriculation[20];
    printf("Entrez l'immatriculation du véhicule à rechercher : ");
    scanf("%s", immatriculation);

    int vehiculeTrouve = 0;

    for (int i = 0; i < ville->nbVehicules; i++) {
        if (strcmp(ville->vehicules[i].immatriculation, immatriculation) == 0) {
            struct Vehicule vehicule = ville->vehicules[i];
            printf("----- Détails du véhicule -----\n");
            printf("Type: %s\n", vehicule.type);
            printf("Immatriculation: %s\n", vehicule.immatriculation);
            printf("Marque: %s\n", vehicule.marque);
            printf("Modèle: %s\n", vehicule.modele);
            printf("Année: %d\n", vehicule.annee);
                    printf("Couleur: %s\n", vehicule.couleur);
            printf("\n");
            vehiculeTrouve = 1;
            break;
        }
    }

    if (!vehiculeTrouve) {
        printf("Aucun véhicule trouvé avec l'immatriculation fournie.\n");
    }
}

// Fonction pour afficher les statistiques par marque
void afficherStatistiquesParMarque(struct Ville* ville) {
    struct Statistiques {
        char marque[50];
        int count;
    };

    struct Statistiques statistiques[MAX_VEHICLES];
    int nbStatistiques = 0;

    for (int i = 0; i < ville->nbVehicules; i++) {
        struct Vehicule vehicule = ville->vehicules[i];
        int marqueTrouvee = 0;

        for (int j = 0; j < nbStatistiques; j++) {
            if (strcmp(vehicule.marque, statistiques[j].marque) == 0) {
                statistiques[j].count++;
                marqueTrouvee = 1;
                break;
            }
        }

        if (!marqueTrouvee) {
            strcpy(statistiques[nbStatistiques].marque, vehicule.marque);
            statistiques[nbStatistiques].count = 1;
            nbStatistiques++;
        }
    }

    printf("Statistiques par marque :\n");
    for (int i = 0; i < nbStatistiques; i++) {
        printf("%s : %d\n", statistiques[i].marque, statistiques[i].count);
    }
}

int main() {
    struct Ville maVille;
    maVille.nbVehicules = 0;

    int choix;
    do {
        printf("##########################################\n");
        printf("----------------------- Menu -----------\n");
        printf("1. Ajouter un véhicule\n");
        printf("2. Supprimer un véhicule\n");
        printf("3. Modifier un véhicule\n");
        printf("4. Afficher tous les véhicules\n");
        printf("5. Rechercher un véhicule\n");
        printf("6. Afficher les statistiques\n");
        printf("7. Quitter\n");
        printf("##########################################\n");
        printf("Votre choix : ");
        scanf("%d", &choix);

        switch (choix) {
            case 1:
                ajouterVehicule(&maVille);
                break;
            case 2:
                supprimerVehicule(&maVille);
                break;
            case 3:
                modifierVehicule(&maVille);
                break;
            case 4:
                afficherTousVehicules(&maVille);
                break;
            case 5:
                rechercherVehicule(&maVille);
                break;
            case 6:
                afficherStatistiquesParMarque(&maVille);
                break;
            case 7:
                printf("Au revoir !\n");
                break;
            default:
                printf("Choix invalide. Veuillez réessayer.\n");
        }

        printf("\n");
    } while (choix != 7);

    return 0;
}

