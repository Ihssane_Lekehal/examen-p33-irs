// Structure représentant un véhicule
struct Vehicule {
    char type[20];
    char immatriculation[20];
    char marque[50];
    char modele[50];
    int annee;
    char couleur[20];
};