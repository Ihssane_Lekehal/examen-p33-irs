// Structure représentant une ville
struct Ville {
    struct Vehicule vehicules[MAX_VEHICLES];
    int nbVehicules;
};