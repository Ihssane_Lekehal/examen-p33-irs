from ClassVehicule import Vehicule
from ClassAutobus import Autobus
from ClassMetro import Metro
from ClassMoto import Moto
from ClassVille import Ville
from ClassVoiture import Voiture


def afficher_menu():
    print("###############################################")
    print("------------------- Menu ----------------------")
    print("1. Ajouter un véhicule")
    print("2. Supprimer un véhicule")
    print("3. Modifier un véhicule")
    print("4. Afficher tous les véhicules")
    print("5. Rechercher un véhicule")
    print("6. Afficher les statistiques")
    print("7. Quitter")
    print("###############################################")


if __name__ == "__main__":
    # Exemple d'utilisation
    ville = Ville()



    ####################################Test de création objet par scripte ~#####################
    # Ajouter des véhicules
    print("###############################################")
    print("Test de création objet par scripte ")
    print("###############################################")
    voiture1 = Voiture("AB123CD", "Renault", "Clio", 2020, "Rouge", 5)
    ville.ajouter_vehicule(voiture1)

    moto1 = Moto("XY456Z", "Honda", "CBR", 2021, "Noire", 600)
    ville.ajouter_vehicule(moto1)

    autobus1 = Autobus("FG789HI", "Mercedes", "Citaro", 2018, "Blanc", 50)
    ville.ajouter_vehicule(autobus1)

    metro1 = Metro("JKL012", "Métro", "M2", 2015, "Gris", "Ligne 2")
    ville.ajouter_vehicule(metro1)

    # Afficher la liste complète des véhicules enregistrés dans la ville
    ville.afficher_vehicules()

    # Rechercher un véhicule par critère
    ville.rechercher_vehicule("Renault")  # Affiche les véhicules de marque Renault

    # Modifier un véhicule
    ville.modifier_vehicule(voiture1, couleur="Bleu")  # Change la couleur de la voiture1 en Bleu

    # Supprimer un véhicule
    ville.supprimer_vehicule(moto1)  # Supprime la moto1 de la liste des véhicules

    # Afficher la liste mise à jour des véhicules enregistrés dans la ville
    ville.afficher_vehicules()


    print("##########################################################")
    print("Test de création objet par l'utilisateur de l'application ")
    print("##########################################################")
    while True:
        afficher_menu()
        choix = input("Sélectionnez une option : ")

        if choix == "1":
            print("----- Ajout d'un véhicule -----")
            type_vehicule = input("Type de véhicule (voiture/moto/autobus/metro) : ")

            immatriculation = input("Immatriculation : ")
            marque = input("Marque : ")
            modele = input("Modèle : ")
            annee = input("Année : ")
            couleur = input("Couleur : ")

            if type_vehicule == "voiture":
                nombre_portes = input("Nombre de portes : ")
                vehicule = Voiture(immatriculation, marque, modele, annee, couleur, nombre_portes)
            elif type_vehicule == "moto":
                cylindree = input("Cylindrée : ")
                vehicule = Moto(immatriculation, marque, modele, annee, couleur, cylindree)
            elif type_vehicule == "autobus":
                nombre_places = input("Nombre de places : ")
                vehicule = Autobus(immatriculation, marque, modele, annee, couleur, nombre_places)
            elif type_vehicule == "metro":
                ligne = input("Ligne : ")
                vehicule = Metro(immatriculation, marque, modele, annee, couleur, ligne)
            else:
                print("Type de véhicule non valide.")
                continue

            ville.ajouter_vehicule(vehicule)
            print("Véhicule ajouté avec succès.")


        elif choix == "2":
            print("----- Suppression d'un véhicule -----")
            immatriculation = input("Immatriculation du véhicule à supprimer : ")
            vehicule_a_supprimer = None

            for vehicule in ville.vehicules:
                if vehicule.immatriculation == immatriculation:
                    vehicule_a_supprimer = vehicule
                    break

            if vehicule_a_supprimer:
                ville.supprimer_vehicule(vehicule_a_supprimer)
                print("Véhicule supprimé avec succès.")
            else:
                print("Aucun véhicule trouvé avec cette immatriculation.")

        elif choix == "3":
            print("----- Modification d'un véhicule -----")
            immatriculation = input("Immatriculation du véhicule à modifier : ")
            vehicule_a_modifier = None

            for vehicule in ville.vehicules:
                if vehicule.immatriculation == immatriculation:
                    vehicule_a_modifier = vehicule
                    break

            if vehicule_a_modifier:
                print("Propriétés du véhicule :")
                vehicule_a_modifier.afficher_info()

                propriete = input("Propriété à modifier (marque/modele/annee/couleur) : ")
                nouvelle_valeur = input("Nouvelle valeur : ")

                ville.modifier_vehicule(vehicule_a_modifier, **{propriete: nouvelle_valeur})
                print("Véhicule modifié avec succès.")

            else:
                print("Aucun véhicule trouvé avec cette immatriculation.")

        elif choix == "4":
            print("----- Liste des véhicules -----")
            ville.afficher_vehicules()

        elif choix == "5":
            print("----- Recherche d'un véhicule -----")
            critere_recherche = input("Entrez un critère de recherche (immatriculation/marque) : ")
            ville.rechercher_vehicule(critere_recherche)

        elif choix == "6":
            ville.afficher_statistiques()

        elif choix == "7":
            print("Programme terminé.")
            break

        else:
            print("Option non valide. Veuillez sélectionner une option valide.")

        print()  # Ligne vide pour une meilleure lisibilité
        