from ClassVehicule import Vehicule
from ClassAutobus import Autobus
from ClassMetro import Metro
from ClassMoto import Moto
from ClassVoiture import Voiture

class Ville:
    def __init__(self):
        self.vehicules = []

    def ajouter_vehicule(self, vehicule):
        self.vehicules.append(vehicule)

    def supprimer_vehicule(self, vehicule):
        self.vehicules.remove(vehicule)

    def modifier_vehicule(self, vehicule, **kwargs):
        for key, value in kwargs.items():
            setattr(vehicule, key, value)

    def afficher_vehicules(self):
        for vehicule in self.vehicules:
            vehicule.afficher_info()

    def rechercher_vehicule(self, critere):
        for vehicule in self.vehicules:
            if critere in vehicule.immatriculation or critere == vehicule.marque:
                vehicule.afficher_info()
    
    def afficher_statistiques(self):
        print("----- Statistiques -----")
        print("1. Afficher le nombre de véhicules par type")
        print("2. Afficher le nombre de véhicules par couleur")
        print("3. Afficher le nombre de véhicules par marque")
        choix = input("Sélectionnez une option : ")

        if choix == "1":
            self.afficher_statistiques_par_type()
        elif choix == "2":
            self.afficher_statistiques_par_couleur()
        elif choix == "3":
            self.afficher_statistiques_par_marque()
        else:
            print("Option non valide.")
        

    def afficher_statistiques_par_type(self):
        type_vehicules = {}
        for vehicule in self.vehicules:
            type_vehicule = type(vehicule).__name__
            if type_vehicule in type_vehicules:
                type_vehicules[type_vehicule] += 1
            else:
                type_vehicules[type_vehicule] = 1

        print("Statistiques par type de véhicule :")
        for type_vehicule, count in type_vehicules.items():
            print(type_vehicule, ":", count)

    def afficher_statistiques_par_couleur(self):
        couleurs = {}
        for vehicule in self.vehicules:
            couleur = vehicule.couleur
            if couleur in couleurs:
                couleurs[couleur] += 1
            else:
                couleurs[couleur] = 1

        print("Statistiques par couleur :")
        for couleur, count in couleurs.items():
            print(couleur, ":", count)

    def afficher_statistiques_par_marque(self):
        marques = {}
        for vehicule in self.vehicules:
            marque = vehicule.marque
            if marque in marques:
                marques[marque] += 1
            else:
                marques[marque] = 1

        print("Statistiques par marque :")
        for marque, count in marques.items():
            print(marque, ":", count)