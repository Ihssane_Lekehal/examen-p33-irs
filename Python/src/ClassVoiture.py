from ClassVehicule import Vehicule


class Voiture(Vehicule):
    def __init__(self, immatriculation, marque, modele, annee, couleur, nombre_portes):
        super().__init__(immatriculation, marque, modele, annee, couleur)
        self.nombre_portes = nombre_portes

    def afficher_info(self):
        super().afficher_info()
        print("Nombre de portes:", self.nombre_portes)