from ClassVehicule import Vehicule

class Metro(Vehicule):
    def __init__(self, immatriculation, marque, modele, annee, couleur, ligne):
        super().__init__(immatriculation, marque, modele, annee, couleur)
        self.ligne = ligne

    def afficher_info(self):
        super().afficher_info()
        print("Ligne:", self.ligne)