Mon application est conçue pour enregistrer différents types de véhicules (voitures, motos, autobus, métros) et générer des statistiques de la circulation routière dans une ville.

Elle comporte plusieurs classes :
La classe "Vehicule" est la classe de base pour tous les types de véhicules et contient des propriétés communes telles que le numéro d’immatriculation, la marque, le modèle, l'année et la couleur.
Les classes spécifiques "Voiture", "Moto", "Autobus" et "Metro" héritent de la classe "Vehicule" et ajoutent des attributs spécifiques à chaque type de véhicule.
Puis la class globale Ville

L'application offre les fonctionnalités suivantes :
jouter un véhicule : L'utilisateur peut ajouter un nouveau véhicule en fournissant les détails appropriés tels que le type de véhicule, le numéro d’immatriculation, la marque, le modèle, l'année, la couleur, et les attributs spécifiques au type de véhicule.
Supprimer un véhicule : L'utilisateur peut supprimer un véhicule en fournissant son numéro d’immatriculation.
Modifier un véhicule : L'utilisateur peut modifier les détails d'un véhicule existant en fournissant son numéro d’immatriculation et en mettant à jour les informations nécessaires.
Afficher tous les véhicules enregistrés : L'utilisateur peut afficher la liste complète des véhicules enregistrés dans la ville, avec tous leurs détails.
Rechercher un véhicule : L'utilisateur peut rechercher un véhicule enregistré en utilisant des critères tels que le numéro d’immatriculation, la marque, etc.
Afficher les statistiques : L'utilisateur peut générer des statistiques sur les véhicules enregistrés dans la ville, telles que le nombre de véhicules par type, par couleur, ou par marque.

