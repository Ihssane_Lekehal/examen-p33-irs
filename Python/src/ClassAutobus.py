from ClassVehicule import Vehicule


class Autobus(Vehicule):
    def __init__(self, immatriculation, marque, modele, annee, couleur, nombre_places):
        super().__init__(immatriculation, marque, modele, annee, couleur)
        self.nombre_places = nombre_places

    def afficher_info(self):
        super().afficher_info()
        print("Nombre de places:", self.nombre_places)
