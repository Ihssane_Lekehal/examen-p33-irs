from ClassVehicule import Vehicule

class Moto(Vehicule):
    def __init__(self, immatriculation, marque, modele, annee, couleur, cylindree):
        super().__init__(immatriculation, marque, modele, annee, couleur)
        self.cylindree = cylindree

    def afficher_info(self):
        super().afficher_info()
        print("Cylindrée:", self.cylindree)