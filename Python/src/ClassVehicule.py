class Vehicule:
    def __init__(self, immatriculation, marque, modele, annee, couleur):
        self.immatriculation = immatriculation
        self.marque = marque
        self.modele = modele
        self.annee = annee
        self.couleur = couleur

    def afficher_info(self):
        print("Immatriculation:", self.immatriculation)
        print("Marque:", self.marque)
        print("Modèle:", self.modele)
        print("Année:", self.annee)
        print("Couleur:", self.couleur)


    